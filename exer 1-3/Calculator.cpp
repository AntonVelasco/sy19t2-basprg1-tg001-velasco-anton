#include <iostream>

using namespace std;

int main()
{
	int x, y;

	cout << "Input x:" << endl;
	cin >> x;
	cout << "Input y:" << endl;
	cin >> y;

	int add = x + y;
	cout << x << "+" << y << "=" << add << endl;
	int minus = x - y;
	cout << x << "-" << y << "=" << minus << endl;

	int multiply = x * y;
	cout << x << "*" << y << "=" << multiply << endl;

	int divide = x / y;
	cout << x << "/" << y << "=" << divide << endl;

	int percent = x % y;
	cout << x << "%" << y << "=" << percent << endl;

	system("pause");
	return 0;
}