#include <iostream>  
using namespace std;

int main()
{
	int i,	factorial = 1, num;
	cout << "Enter Number: ";
	cin >> num;
	for (i = 1; i <= num; i++) 
	{
		factorial = factorial * i;
	}
	cout << "Factorial of " << num << " is: " << factorial << endl;
	system("pause");
	return 0;
}