#include <iostream>

using namespace std;

int main()
{
	int radius;
	//input
	cout << "Radius=";
	cin >>radius;

	//computation
	float comarea = (3.14)*(radius*radius);
	float circumference =( 2 * (3.14)*radius);
	//display
	cout <<"Area ="<<comarea << endl;
	cout << "Circumference =" << circumference << endl;
	system("pause");
	return 0;
}
